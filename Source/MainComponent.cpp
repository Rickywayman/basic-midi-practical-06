/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    audioDeviceManager.setMidiInputEnabled("USB Axiom 49 Port 1", true);
    audioDeviceManager.addMidiInputCallback(String::empty, this);
    addAndMakeVisible(&midiLabel);
    midiLabel.setText(String::empty, dontSendNotification); //
}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
}

void MainComponent::resized()
{
    midiLabel.setBounds(0, 0, 200, 200);

}
void MainComponent::handleIncomingMidiMessage (MidiInput* , const MidiMessage& message)
{
    DBG("MIDI");
    
    String midiText;
    
    if (message.isNoteOn())
    {
        midiText << "NoteOn: Channel " << message.getChannel();
        midiText << ":Number" << message.getNoteNumber();
        midiText << ":Velocity" << message.getVelocity();
    }
    
    midiLabel.getTextValue() = midiText;
}